import time
from string import printable
import matplotlib.pyplot as plt


CYRILLIC_LETTERS = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
SPECIAL = "«—–»"
SYMBOLS = CYRILLIC_LETTERS + CYRILLIC_LETTERS.upper() + printable + SPECIAL
SYMBOLS_COUNT = len(SYMBOLS)
CRYPT_MODES = {
    "encrypt": "encrypt",
    "decrypt": "decrypt",
    "attack": "attack",
    "generate": "generate",
}


class DataReader:
    def read_data_from_file(self, file, mode="r"):
        with open(file, mode) as f:
            return f.read()

    def save_data_to_file(self, data, file, mode="w"):
        with open(file, mode) as f:
            f.write(data)


class Analyzer:
    def get_letter_statistics(self, data):
        result = {}
        for letter in data:
            if not letter.lower() in CYRILLIC_LETTERS:
                continue
            if not result.get(letter):
                result[letter] = 0
            result[letter] += 1
        return {item[0]: item[1] for item in sorted(
            result.items(), key=(lambda x: -x[1]))}

    def get_code_statistics(self, data):
        result = dict()
        for symbol in data:
            symbol = chr(symbol)
            if not result.get(symbol):
                result[symbol] = 0
            result[symbol] += 1
        return {item[0]: item[1] for item in sorted(
            result.items(), key=(lambda x: -x[1]))}

    def draw_graphic(self, data: list):
        fig, ax = plt.subplots(2, 1)
        fig.set_figwidth(18)
        fig.set_figheight(8)
        for index, item in enumerate(data):
            ax[index].bar(item.keys(), item.values())
            ax[index].grid()
        plt.show()


def timer(foo):
    def inner(*args, **kwargs):
        start = time.time()
        foo(*args, **kwargs)
        print("Времени затрачено: {time:.2f} с.".format(
            time=time.time() - start))
    return inner
